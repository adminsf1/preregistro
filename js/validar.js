/*
	Valida si seleccionan el tipo de documento NIT que el contenido ingresado
	sea solo numérico y tenga exactamente 9 digitos
 */

function validarNIT(evt){
	var tipo = document.getElementById('00N1N00000HzAnG').value;
	if( tipo == 'NIT' ){
		var doc = document.getElementById('00N1N00000HzAnF').value;

		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57) || doc.toString().length === 9)
		    return false;
		
		return true;
	}	
}
/*
	Muestra modal de nit sin número de verficación y limpia el número del documento
 */
function limpiarIdentificacion(){
	var tipo = document.getElementById('00N1N00000HzAnG').value;
	var delay = 7000;
	if( tipo == 'NIT' ){
		$('#div_validacionnit').show();		
		document.getElementById('00N1N00000HzAnF').value = null;
		setTimeout(function() {            
            $('[id$=div_validacionnit]').hide();	
        }, delay);
	}else{
		$('#div_validacionnit').hide();	
		document.getElementById('00N1N00000HzAnF').value = null;
	}
}

/*
	Valida que el número del nit tenga exactamente 9 digitos si es así deja guardar
	sino muestra un modal advirtiendo del error
 */

function validarCampoNit(){
	debugger;
	var tipo = document.getElementById('00N1N00000HzAnG').value;
	if( tipo == 'NIT' ){
		var doc = document.getElementById('00N1N00000HzAnF').value;
		
		if( doc.length != 9 ){
			$('#modalFormNIT').modal('show');
			return false;
		}
	}	
}